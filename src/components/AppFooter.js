import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="/dashboard" target="_blank" rel="noopener noreferrer">
          Hospital Mgmt System
        </a>
        <span className="ms-1">&copy; 2023 BITS-Pilani.</span>
      </div>
      <div className="ms-auto">
        <span className="me-1">Designed by</span>
        <a href="mailto:2021MT93102@wilp.bits-pilani.ac.in">Dineshkumar</a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
