import React, { useEffect, useState } from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
} from '@coreui/react'

const DoctorsListPage = () => {
  const [users, setUsers] = useState([])
  console.log('Print the responses')
  const fetchUserData = () => {
    fetch('http://localhost:5800/api/staff/all')
      .then((response) => {
        const responseData = response.json()
        console.log(responseData)
        return responseData
      })
      .then((data) => {
        console.log(data)
        setUsers(data)
      })
  }
  useEffect(() => {
    fetchUserData()
  }, [])
  let counter = 1
  return (
    <div>
      <CTable striped hover bordered responsive>
        {users.length > 0 && (
          <CTableHead color="light">
            <CTableRow>
              <CTableHeaderCell scope="col">S.#</CTableHeaderCell>
              <CTableHeaderCell scope="col">First Name</CTableHeaderCell>
              <CTableHeaderCell scope="col">Last Name</CTableHeaderCell>
              <CTableHeaderCell scope="col">Gender</CTableHeaderCell>
              <CTableHeaderCell scope="col">Age</CTableHeaderCell>
              <CTableHeaderCell scope="col">Address Line 1</CTableHeaderCell>
              <CTableHeaderCell scope="col">Address Line 2</CTableHeaderCell>
              <CTableHeaderCell scope="col">Phone Number</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
        )}
        {users.length > 0 && (
          <CTableBody>
            {users.map((user) => (
              <CTableRow key={user.id}>
                <CTableHeaderCell scope="row">{counter++}</CTableHeaderCell>
                <CTableDataCell>{user.first_name}</CTableDataCell>
                <CTableDataCell>{user.last_name}</CTableDataCell>
                <CTableDataCell>{user.gender}</CTableDataCell>
                <CTableDataCell>{user.patient_age}</CTableDataCell>
                <CTableDataCell>{user.address_line_1}</CTableDataCell>
                <CTableDataCell>{user.address_line_2}</CTableDataCell>
                <CTableDataCell>{user.phone_number}</CTableDataCell>
              </CTableRow>
            ))}
          </CTableBody>
        )}
      </CTable>
    </div>
  )
}

export default DoctorsListPage
