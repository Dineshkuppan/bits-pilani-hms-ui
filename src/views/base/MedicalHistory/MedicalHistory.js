import React, { useEffect, useState } from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
} from '@coreui/react'

const MedicalHistory = () => {
  const [users, setUsers] = useState([])
  console.log('Print the responses')
  const fetchUserData = () => {
    fetch('http://localhost:5800/api/medical-history/all')
      .then((response) => {
        const responseData = response.json()
        console.log(responseData)
        return responseData
      })
      .then((data) => {
        console.log(data)
        setUsers(data)
      })
  }
  useEffect(() => {
    fetchUserData()
  }, [])
  let counter = 1
  return (
    <div>
      <CTable striped hover bordered responsive>
        {users.length > 0 && (
          <CTableHead color="light">
            <CTableRow>
              <CTableHeaderCell scope="col">S.#</CTableHeaderCell>
              <CTableHeaderCell scope="col">Disease Name</CTableHeaderCell>
              <CTableHeaderCell scope="col">Diagnosed Date</CTableHeaderCell>
              <CTableHeaderCell scope="col">Description</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
        )}
        {users.length > 0 && (
          <CTableBody>
            {users.map((user) => (
              <CTableRow key={user.id}>
                <CTableHeaderCell scope="row">{counter++}</CTableHeaderCell>
                <CTableDataCell>{user.disease_name}</CTableDataCell>
                <CTableDataCell>{user.diagnosed_date}</CTableDataCell>
                <CTableDataCell>{user.description}</CTableDataCell>
              </CTableRow>
            ))}
          </CTableBody>
        )}
      </CTable>
    </div>
  )
}

export default MedicalHistory
