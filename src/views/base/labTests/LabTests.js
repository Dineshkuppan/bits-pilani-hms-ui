import React, { useEffect, useState } from 'react'
import {
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
} from '@coreui/react'

const LabTests = () => {
  const [users, setUsers] = useState([])
  console.log('Print the responses')
  const fetchUserData = () => {
    fetch('http://localhost:5800/api/labtests/all')
      .then((response) => {
        const responseData = response.json()
        console.log(responseData)
        return responseData
      })
      .then((data) => {
        console.log(data)
        setUsers(data)
      })
  }
  useEffect(() => {
    fetchUserData()
  }, [])
  let counter = 1
  return (
    <div>
      <CTable striped hover bordered responsive>
        {users.length > 0 && (
          <CTableHead color="light">
            <CTableRow>
              <CTableHeaderCell scope="col">S.#</CTableHeaderCell>
              <CTableHeaderCell scope="col">Test Type</CTableHeaderCell>
              <CTableHeaderCell scope="col">Patient ID</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
        )}
        {users.length > 0 && (
          <CTableBody>
            {users.map((user) => (
              <CTableRow key={user.id}>
                <CTableHeaderCell scope="row">{counter++}</CTableHeaderCell>
                <CTableDataCell>{user.test_type}</CTableDataCell>
                <CTableDataCell>{user.patient_id}</CTableDataCell>
              </CTableRow>
            ))}
          </CTableBody>
        )}
      </CTable>
    </div>
  )
}

export default LabTests
