import React from 'react'
import { CContainer, CRow } from '@coreui/react'

const Dashboard = () => {
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center"></CRow>
      </CContainer>
    </div>
  )
}

export default Dashboard
