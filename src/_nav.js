import React from 'react'
import CIcon from '@coreui/icons-react'
import { cilPuzzle, cilSpeedometer } from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Hospital Management',
  },
  {
    component: CNavGroup,
    name: 'Admin',
    to: '/base',
    icon: <CIcon icon={cilPuzzle} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Patient',
        to: '/base/patient',
      },
      {
        component: CNavItem,
        name: 'Doctors',
        to: '/base/doctors',
      },
      {
        component: CNavItem,
        name: 'Hospital Staff',
        to: '/base/hospitalStaff',
      },
      {
        component: CNavItem,
        name: 'Medical History',
        to: '/base/MedicalHistory',
      },
      {
        component: CNavItem,
        name: 'Bill Summary',
        to: '/base/billSummary',
      },
      {
        component: CNavItem,
        name: 'Payment(s)',
        to: '/base/payment',
      },
      {
        component: CNavItem,
        name: 'Lab Tests',
        to: '/base/labTests',
      },
      {
        component: CNavItem,
        name: 'Drugs',
        to: '/base/drugs',
      },
    ],
  },
]

export default _nav
